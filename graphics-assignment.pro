#-------------------------------------------------
#
# Project created by QtCreator 2017-01-23T18:22:01
#
#-------------------------------------------------

QT  += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET		= graphics-assignment
TEMPLATE	= app


SOURCES	+= main.cpp\
        glslprogram.cpp \
        glutils.cpp \
        mainview.cpp \
        mainwindow.cpp \
        scenebasic.cpp \
        pointsetter.cpp \
    anglesetter.cpp \
    linerotationcontrols.cpp \
    setdefault.cpp \
    viewpositioncontrols.cpp

HEADERS += \
        glslprogram.h \
        glutils.h \
        mainview.h \
        mainwindow.h \
        scene.h \
        scenebasic.h \
        ui_mainwindow.h \
        point.h \
        pointsetter.h \
    anglesetter.h \
    linerotationcontrols.h \
    setdefault.h \
    viewpositioncontrols.h

FORMS	+= mainwindow.ui

win32: LIBS += -L$$PWD/./lib/ -lglew32 -lopengl32 -lglu32
