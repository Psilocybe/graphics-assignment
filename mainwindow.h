#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "mainview.h"
#include "linerotationcontrols.h"
#include "viewpositioncontrols.h"
#include "setdefault.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

private:
    Ui::MainWindow *ui;
    MainView* view;
    LineRotationControls* lineRotationControls;
    ViewPositionControls* viewPositionControls;

    void setConnections();

public:
    explicit MainWindow(QWidget *parent = 0);
    void setView(MainView* view);

    ~MainWindow();

private slots:
    void on_set_position_triggered();
    void on_set_default_position_triggered();
    void on_line_rotation_triggered();
    void on_line_default_rotation_triggered();
};

#endif // MAINWINDOW_H
