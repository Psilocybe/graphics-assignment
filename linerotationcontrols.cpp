#include "linerotationcontrols.h"

#include <QGroupBox>
#include <QHBoxLayout>
#include <QVBoxLayout>

LineRotationControls::LineRotationControls(QWidget *parent)
    : QWidget(parent)
{
    this->build();
}

void LineRotationControls::build()
{
    auto groupBox = new QGroupBox("Line Rotation");
    auto vLayout = new QVBoxLayout;
    groupBox->setLayout(vLayout);

    auto hLayout = new QHBoxLayout;
    this->pointSetter_b = new PointSetter("B", this);
    this->pointSetter_d = new PointSetter("D", this);
    this->setDefaultButtons = new SetDefault(this);

    hLayout->addWidget(pointSetter_b);
    hLayout->addWidget(pointSetter_d);
    this->angleSetter = new AngleSetter("Rotation", this);

    vLayout->addLayout(hLayout);
    vLayout->addWidget(this->angleSetter);
    vLayout->addWidget(this->setDefaultButtons);
    groupBox->setLayout(vLayout);
    auto layout = new QVBoxLayout;
    layout->addWidget(groupBox);

    this->setLayout(layout);
}

double LineRotationControls::getAngle() const
{
    return this->angleSetter->getAngle();
}

glm::vec3 LineRotationControls::getBPoint() const
{
    return this->pointSetter_b->getPoint();
}

glm::vec3 LineRotationControls::getDPoint() const
{
    return this->pointSetter_d->getPoint();
}

QPushButton* LineRotationControls::getSetButton() const
{
    return this->setDefaultButtons->getSetButton();
}

QPushButton* LineRotationControls::getDefaultButton() const
{
    return this->setDefaultButtons->getDefaultButton();
}
