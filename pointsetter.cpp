#include "pointsetter.h"

#include <QFormLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QGroupBox>
#include <limits>

PointSetter::PointSetter(const char *label)
    : PointSetter(label, 0)
{
}

PointSetter::PointSetter(const char *label, QWidget *parent)
    : QWidget(parent)
{
    this->build(label);
}

glm::vec3 PointSetter::getPoint() const
{
    return glm::vec3(xSpin->value(), ySpin->value(), zSpin->value());
}

void PointSetter::build(const char *label)
{
    auto groupBox = new QGroupBox(label);
    auto formLayout = new QFormLayout;
    groupBox->setLayout(formLayout);

    this->xSpin = this->createSpinBox(-1000, 1000);
    this->ySpin = this->createSpinBox(-1000, 1000);
    this->zSpin = this->createSpinBox(-1000, 1000);

    formLayout->addRow(new QLabel("X"), this->xSpin);
    formLayout->addRow(new QLabel("Y"), this->ySpin);
    formLayout->addRow(new QLabel("Z"), this->zSpin);

    auto layout = new QHBoxLayout;
    layout->addWidget(groupBox);

    this->setLayout(layout);
}

QDoubleSpinBox *PointSetter::createSpinBox(double min, double max)
{
    auto spiner = new QDoubleSpinBox;
    spiner->setMinimum(min);
    spiner->setMaximum(max);
    return spiner;
}
