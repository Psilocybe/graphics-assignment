#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDesktopWidget>
#include <QSplitter>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QSizePolicy>
#include <iostream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

void MainWindow::setView(MainView *view)
{
    this->view = view;
    this->lineRotationControls = new LineRotationControls(this);
    this->viewPositionControls = new ViewPositionControls(this);

    auto vLayout = new QVBoxLayout;
    vLayout->addWidget(lineRotationControls);
    vLayout->addWidget(viewPositionControls);

    auto hLayout = new QHBoxLayout;
    QSizePolicy leftPolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    leftPolicy.setHorizontalStretch(8);
    view->setSizePolicy(leftPolicy);

    hLayout->addWidget(view);
    hLayout->addLayout(vLayout);

    QWidget *widget = new QWidget();
    widget->setLayout(hLayout);

    setCentralWidget(widget);

    this->resize(QDesktopWidget().availableGeometry(this).size() * 0.7f);

    this->setConnections();
}

void MainWindow::setConnections()
{
   auto button = this->viewPositionControls->getSetButton();
   connect(button, SIGNAL (released()), this, SLOT (on_set_position_triggered()));
   button = this->viewPositionControls->getDefaultButton();
   connect(button, SIGNAL (released()), this, SLOT (on_set_default_position_triggered()));
   button = this->lineRotationControls->getSetButton();
   connect(button, SIGNAL (released()), this, SLOT (on_line_rotation_triggered()));
   button = this->lineRotationControls->getDefaultButton();
   connect(button, SIGNAL (released()), this, SLOT (on_line_default_rotation_triggered()));
}

void MainWindow::on_line_rotation_triggered()
{
    auto b = this->lineRotationControls->getBPoint();
    auto d = this->lineRotationControls->getDPoint();
    auto angle = this->lineRotationControls->getAngle();
    view->rotateModel(b, d, angle);
}

void MainWindow::on_line_default_rotation_triggered()
{
    view->resetModel();
}

void MainWindow::on_set_position_triggered()
{
    auto eye = this->viewPositionControls->getEyePoint();
    auto direction = this->viewPositionControls->getDirectionPoint();
    view->updateView(eye, direction);
}

void MainWindow::on_set_default_position_triggered()
{
    auto eye = glm::vec3(0.0, 0.0, 2.0);
    auto direction = glm::vec3(0.0, 0.0, -1.0);
    view->updateView(eye, direction);
}

MainWindow::~MainWindow()
{
    delete ui;
}
