#ifndef POINTSETTER_H
#define POINTSETTER_H

#include <QWidget>
#include <QDoubleSpinBox>

#include "C:/Utils/glm/glm/vec3.hpp"

class PointSetter
        : public QWidget
{
    Q_OBJECT
private:
    QDoubleSpinBox *xSpin;
    QDoubleSpinBox *ySpin;
    QDoubleSpinBox *zSpin;

    void build(const char *label);
    QDoubleSpinBox *createSpinBox(double min, double max);

public:
    explicit PointSetter(const char *label);
    explicit PointSetter(const char *label, QWidget *parent = 0);

    glm::vec3 getPoint() const;

signals:

public slots:
};

#endif // POINTSETTER_H
