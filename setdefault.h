#ifndef SETDEFAULT_H
#define SETDEFAULT_H

#include <QWidget>
#include <QPushButton>

class SetDefault : public QWidget
{
    Q_OBJECT
private:
    QPushButton * setButton;
    QPushButton * defaultButton;

    void build();

public:
    explicit SetDefault(QWidget *parent = 0);
    QPushButton* getSetButton() const;
    QPushButton* getDefaultButton() const;

signals:

public slots:
};

#endif // SETDEFAULT_H
