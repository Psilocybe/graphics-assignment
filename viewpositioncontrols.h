#ifndef VIEWPOSITIONCONTROLS_H
#define VIEWPOSITIONCONTROLS_H

#include <QWidget>

#include "pointsetter.h"
#include "setdefault.h"

class ViewPositionControls : public QWidget
{
    Q_OBJECT
private:
    PointSetter* pointSetter_eye;
    PointSetter* pointSetter_direction;
    SetDefault* setDefaultButtons;

    void build();

public:
    explicit ViewPositionControls(QWidget *parent = 0);

    glm::vec3 getEyePoint() const;
    glm::vec3 getDirectionPoint() const;
    QPushButton* getSetButton() const;
    QPushButton* getDefaultButton() const;

signals:

public slots:
};

#endif // VIEWPOSITIONCONTROLS_H
