#ifndef MAINVIEW_H
#define MAINVIEW_H

#include "C:/Utils/glew-2.0.0/include/GL/glew.h"
#include "C:/Utils/glm/glm/glm.hpp"
#include <QGLWidget>

using glm::vec3;
#include "scene.h"

class MainView : public QGLWidget
{
    Q_OBJECT

private:
    Scene *scene;
    float angle;
    vec3 axis;

public:
    MainView( const QGLFormat & format, QWidget *parent = 0 );

    void setRotAxis(float ang, float x, float y , float z);
    void updateView(glm::vec3 eye, glm::vec3 direction);
    void resetModel();
    void MainView::rotateModel(glm::vec3 b, glm::vec3 d, float phi);

protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int w, int h);

public slots:
    void takeScreenShot();
};

#endif // MAINVIEW_H
