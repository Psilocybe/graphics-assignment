#ifndef SCENE_H
#define SCENE_H

#include "C:/Utils/glm/glm/glm.hpp"

class Scene
{
public:
    /**
      Load textures, initialize shaders, etc.
      */
    virtual void initScene() = 0;

    /**
      This is called prior to every frame.  Use this
      to update your animation.
      */
    virtual void update(float t) = 0;

    /**
      Draw your scene.
      */
    virtual void render() = 0;

    /**
      Called when screen is resized
      */
    virtual void resize(int, int) = 0;

    /**
      Set axis angle.
    */
    virtual void setAngleAxis(float angle, glm::vec3 axis) = 0;

    virtual void resetModel() = 0;

    virtual void updateView(glm::vec3 eye, glm::vec3 direction) = 0;

    virtual void rotateModel(glm::vec3 b, glm::vec3 d, float phi) = 0;
};

#endif // SCENE_H
