#include "anglesetter.h"

#include <QLabel>
#include <QGroupBox>
#include <QFormLayout>
#include <QHBoxLayout>

AngleSetter::AngleSetter(const char *label)
    : AngleSetter(label, 0)
{
}

AngleSetter::AngleSetter(const char *label, QWidget *parent)
    : QWidget(parent)
{
    this->build(label);
}

void AngleSetter::build(const char *label)
{
    auto groupBox = new QGroupBox(label);
    auto formLayout = new QFormLayout;
    groupBox->setLayout(formLayout);
    auto hLayout = new QHBoxLayout;

    this->angleSlider = this->createSlider(0.0, 360.0);
    this->angleLabel = new QLabel(this);
    this->angleLabel->setText(QString::number(angleSlider->value()) + tr("°"));
    hLayout->addWidget(angleSlider);
    hLayout->addWidget(angleLabel);
    formLayout->addRow(new QLabel("Angle"), hLayout);

    this->signalMapper = new QSignalMapper(this);
    connect(this->angleSlider, SIGNAL(valueChanged(int)), this->signalMapper, SLOT(map()));
    this->signalMapper->setMapping(this->angleSlider, this->angleLabel);
    connect(this->signalMapper, SIGNAL(mapped(QWidget*)),this, SLOT(onSliderMapped(QWidget*)));

    auto layout = new QHBoxLayout;
    layout->addWidget(groupBox);

    this->setLayout(layout);
}

void AngleSetter::onSliderMapped(QWidget *widget)
{
    auto slider = qobject_cast<QSlider*>(this->signalMapper->mapping(widget));
    auto label = qobject_cast<QLabel*>(widget);
    if (label && slider) {
        label->setText(QString::number(slider->value()) + tr("°"));
    }
}

QSlider* AngleSetter::createSlider(double min, double max)
{
    auto slider = new QSlider(Qt::Orientation::Horizontal);
    slider->setMinimum(min);
    slider->setMaximum(max);
    slider->setValue((double)(max - min) / 2);
    return slider;
}

double AngleSetter::getAngle() const
{
    return this->angleSlider->value();
}
