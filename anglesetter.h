#ifndef ANGLESETTER_H
#define ANGLESETTER_H

#include <QLabel>
#include <QWidget>
#include <QSlider>
#include <QSignalMapper>

class AngleSetter
        : public QWidget
{
    Q_OBJECT
private:
    QSlider *angleSlider;
    QLabel *angleLabel;
    QSignalMapper *signalMapper;

    void build(const char *label);
    QSlider *createSlider(double min, double max);

public:
    explicit AngleSetter(const char *label);
    explicit AngleSetter(const char *label, QWidget *parent = 0);

    double getAngle() const;
signals:

public slots:
    void onSliderMapped(QWidget *widget);
};

#endif // ANGLESETTER_H

