#ifndef SCENEBASIC_H
#define SCENEBASIC_H

#include "C:/Utils/glew-2.0.0/include/GL/glew.h"
#include "C:/Utils/glm/glm/glm.hpp"
#include "glslprogram.h"
#include "scene.h"

using glm::mat4;

class SceneBasic
    : public Scene
{
private:
    int width, height;
    GLuint vaoHandle;

    GLuint rotationLine;
    float linePositon[6];
    float lineColor[6];

    float angle;
    vec3 axis;
    mat4 rotationMatrix;
    GLSLProgram prog;

    mat4 model;
    mat4 view;
    mat4 projection;

    void readData(const char* fname);
    void CreateVBO();

    float positionData[108];
    float colorData[108];
public:
    SceneBasic();
    void setMatrices();

    void initScene();
    void render();
    void resize(int, int);
    void update(float t);
    void setAngleAxis(float ang, vec3 ax);
    void updateView(glm::vec3 eye, glm::vec3 direction);
    void rotateModel(glm::vec3 b, glm::vec3 d, float phi);
    void resetModel();
    void printActiveUniforms(GLuint programHandle);
    void printActiveAttribs(GLuint programHandle);
};

#endif // SCENEBASIC_H
