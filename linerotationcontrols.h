#ifndef LINEROTATIONCONTROLS_H
#define LINEROTATIONCONTROLS_H

#include <QWidget>

#include "anglesetter.h"
#include "pointsetter.h"
#include "setdefault.h"

class LineRotationControls
        : public QWidget
{
    Q_OBJECT
private:
    AngleSetter* angleSetter;
    PointSetter* pointSetter_b;
    PointSetter* pointSetter_d;
    SetDefault* setDefaultButtons;

    void build();
public:
    explicit LineRotationControls(QWidget *parent = 0);

    double getAngle() const;
    glm::vec3 getBPoint() const;
    glm::vec3 getDPoint() const;
    QPushButton* getSetButton() const;
    QPushButton* getDefaultButton() const;

signals:

public slots:
};

#endif // LINEROTATIONCONTROLS_H
