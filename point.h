#ifndef POINT_H
#define POINT_H

struct Point
{
    double x;
    double y;
    double z;
    Point(double x, double y, double z)
        : x(x), y(y), z(z)
    {}
};

#endif // POINT_H
