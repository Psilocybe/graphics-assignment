#include "viewpositioncontrols.h"

#include <QGroupBox>
#include <QHBoxLayout>
#include <QVBoxLayout>

ViewPositionControls::ViewPositionControls(QWidget *parent)
    : QWidget(parent)
{
    this->build();
}

void ViewPositionControls::build()
{
    auto groupBox = new QGroupBox("View Position");
    auto vLayout = new QVBoxLayout;
    groupBox->setLayout(vLayout);

    auto hLayout = new QHBoxLayout;
    this->pointSetter_eye = new PointSetter("Eye", this);
    this->pointSetter_direction = new PointSetter("Direction", this);
    this->setDefaultButtons = new SetDefault(this);
    hLayout->addWidget(this->pointSetter_eye);
    hLayout->addWidget(this->pointSetter_direction);

    hLayout->addWidget(this->setDefaultButtons);

    vLayout->addLayout(hLayout);
    vLayout->addWidget(this->setDefaultButtons);
    auto layout = new QVBoxLayout;
    layout->addWidget(groupBox);

    this->setLayout(layout);
}

glm::vec3 ViewPositionControls::getEyePoint() const
{
    return this->pointSetter_eye->getPoint();
}

glm::vec3 ViewPositionControls::getDirectionPoint() const
{
    return this->pointSetter_direction->getPoint();
}

QPushButton* ViewPositionControls::getSetButton() const
{
    return this->setDefaultButtons->getSetButton();
}

QPushButton* ViewPositionControls::getDefaultButton() const
{
    return this->setDefaultButtons->getDefaultButton();
}
