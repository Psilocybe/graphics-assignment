#include "setdefault.h"

#include <QHBoxLayout>
#include <QGroupBox>

SetDefault::SetDefault(QWidget *parent)
    : QWidget(parent)
{
    build();
}

QPushButton* SetDefault::getSetButton() const
{
    return this->setButton;
}

QPushButton* SetDefault::getDefaultButton() const
{
    return this->defaultButton;
}

void SetDefault::build()
{
    auto hLayout = new QHBoxLayout;

    this->setButton = new QPushButton("Set");
    this->defaultButton = new QPushButton("Default");

    hLayout->addWidget(setButton);
    hLayout->addWidget(defaultButton);

    this->setLayout(hLayout);
}
