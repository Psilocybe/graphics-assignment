#include "mainwindow.h"
#include <QApplication>
#include <QWidget>
#include <QMainWindow>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QAction>
#include <QMenu>
#include <QMenuBar>
#include <QGLFormat>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    auto window = new MainWindow();
    QGLFormat format;
    format.setVersion(4,0);
    format.setProfile(QGLFormat::CoreProfile);
    auto glView = new MainView(format, window);
    window->setView(glView);
    window->show();
    return a.exec();
}
