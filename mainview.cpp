#include "mainview.h"
#include "scenebasic.h"
#include "glutils.h"
#include <iostream>
#include <cstdio>

MainView::MainView(const QGLFormat & format, QWidget *parent) : QGLWidget(format, parent)
{
    this->setFocusPolicy(Qt::ClickFocus);
    this->setMinimumSize(800,600);
}

void MainView::initializeGL()
{
    scene = new SceneBasic();

    GLenum err = glewInit();
    if( GLEW_OK != err ) {
        std::cout << "Error initializing GLEW: " << glewGetErrorString(err) << std::endl;
    }
    GLUtils::checkForOpenGLError();

    QGLFormat format = this->format();
    printf("QGLFormat reports profile: ");
    if (format.profile() == QGLFormat::CompatibilityProfile) {
        std::cout << "compatability." << std::endl;
    } else if( format.profile() == QGLFormat::CoreProfile ) {
        std::cout << "core." << std::endl;
    } else {
        std::cout << "none." << std::endl;
    }
    GLUtils::dumpGLInfo();

    glClearColor(0.0f,0.0f,0.0f,1.0f);

    scene->initScene();
}

void MainView::paintGL()
{
    GLUtils::checkForOpenGLError();
    scene->render();
}

void MainView::resizeGL(int w, int h)
{
    scene->resize(w,h);
}

void MainView::takeScreenShot()
{
    QImage img = this->grabFrameBuffer(true);
    img.save("screen.png", "PNG");
}

void MainView::setRotAxis(float ang, float x, float y , float z)
{
    scene->setAngleAxis(ang, vec3(x,y,z));
    updateGL();
}

void MainView::updateView(glm::vec3 eye, glm::vec3 direction)
{
    scene->updateView(eye, direction);
    updateGL();
}

void MainView::resetModel()
{
    scene->resetModel();
    updateGL();
}

void MainView::rotateModel(glm::vec3 b, glm::vec3 d, float phi)
{
    scene->rotateModel(b, d, phi);
    updateGL();
}
